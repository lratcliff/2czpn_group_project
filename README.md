# 2CzPN Group Project for CHEM20008

This project provides the data needed for the Predictions and Modelling Group Project on _Exploring the Relationship Between Atomic and Electronic Structure in an OLED Molecule_.

## Description

- Atomic structures for all the molecules are given in _extracted\_structures_
- The folder _torsion\_angles_ contains a text file giving the torsion angles for the extracted structures
- The folder _valence_ contains text files with the LDA-calculated total energies and frontier orbital energies for the different sets of molecules
- The folder _core_ contains text files with the LDA-calculated N 1 _s_ core binding energies
- The file _project\_description.pdf_ provides the project description

## Origin of Data

The data provided here is related to that in the paper _Probing Disorder in 2CzPN using Core and Valence States_, N. K. Fernando, M. Stella, W. Dawson, T. Nakajima, L. Genovese, A. Regoutz and L. E. Ratcliff, Phys. Chem. Chem. Phys., 24, 23329 (2022), which can be found at https://pubs.rsc.org/en/content/articlehtml/2022/cp/d2cp02638d. The original repository associated with the paper can be found at https://gitlab.com/lratcliff/2czpn.

